<?php
/**
 * @file
 * Join handler for revisioned fields.
 */

/**
 * Join handler class
 */
class views_handler_join_field_node_revision extends views_join {
  /**
   * Override build_join().
   *
   * Adds nid condition.
   */
  public function build_join($select_query, $table, $view_query) {
    $view_query->add_where_expression(0, $table['alias'] . '.entity_id = ' . $this->left_table . '.nid');
    parent::build_join($select_query, $table, $view_query);
  }
}
